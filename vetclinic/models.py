from django.db import models
from django.db.models import Model
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import os
# Create your models here.
from lapushka.settings import MEDIA_ROOT, USE_JSON_DATA_LAYER

if USE_JSON_DATA_LAYER:
    from tinydb import TinyDB, Query
    db = TinyDB('/home/zhblnd/db.json')

class BaseModel(models.Model):

    class Meta:
        abstract=True # Set this model as Abstract

    def __json__(self):
        print("A")

    def save(self, *args, **kwargs):
        print("SAVE NETHID OF BASEMODEL")
        if USE_JSON_DATA_LAYER:
            self._save()
        else:
            # self._meta.model.save()
            # super(BaseModel, self).save(*args, **kwargs)
            # models.Model.save(self, *args, **kwargs)
            # super(BaseModel, self).save(*args, **kwargs)
            Model.save(self)
            # super(BaseModel, self).save()

    def get_all(self):
        if USE_JSON_DATA_LAYER:
            # print(db.table(self._meta.model_name))
            return self._get_all()
        else:
            return self._meta.model.objects.all()


class Animal(BaseModel):
    """
    Модель, описывающая тип животного
    """
    name = models.CharField(max_length=200, help_text="Укажите вид вашего животного. Например, собака, кошка и т.п")


    def __str__(self):
        """
        Строка для описания объекта модели (in Admin site etc.)
        """
        return self.name

class Pet(BaseModel):
    """
    Модель, описывающая домашнее животное
    """
    name = models.CharField(max_length=200, help_text="Укажите имя вашего животного. Например, собака, кошка и т.п")
    type = models.ForeignKey('Animal', on_delete=models.SET_NULL, null=True)
    date_of_birth = models.DateField(null=True, blank=True)

    def __str__(self):
        """
        Строка для описания объекта модели.
        """
        return self.name

class Profile(BaseModel):
    """
    Модель, описывающая пользователя сайта.
    """
    name = models.CharField(max_length=200, default="")
    email = models.EmailField(default="")
    phone_number = PhoneNumberField()
    pet = models.ForeignKey('Pet', on_delete=models.SET_NULL, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        """
        Строка для описания объекта модели.
        """
        return self.name +" "+ self.email


@receiver(post_save, sender=User)
def new_user(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

class Specialization(BaseModel):
    """
    Модель, описывающая специальность врача.
    """
    name = models.CharField(max_length=200)

    def __str__(self):
        """
        Строка для описания объекта модели.
        """
        return self.name

class VetDoctor(BaseModel):
    """
    Модель, описывающая ветеринара.
    """
    name = models.CharField(max_length=200)
    specialization = models.ForeignKey('Specialization', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        """
        Строка для описания объекта модели.
        """
        return self.name


class Shedule(BaseModel):
    monday = models.CharField(max_length=20)
    tuesday = models.CharField(max_length=20)
    wednesday = models.CharField(max_length=20)
    thursday = models.CharField(max_length=20)
    friday = models.CharField(max_length=20)
    saturday = models.CharField(max_length=20)
    sunday = models.CharField(max_length=20)

class Document(BaseModel):
    docfile = models.FileField(upload_to='documents/')

    def delete(self, *args, **kwargs):
        os.remove(os.path.join(MEDIA_ROOT, self.docfile.name))
        super(Document, self).delete(*args, **kwargs)

    def _save(self, *args, **kwargs):
        print("SAVE METHOD OF DOCUMENT")
        table = db.table('Document')
        table.insert(dict((self.__json__())))
        full_filename = os.path.join(MEDIA_ROOT, 'documents/', self.docfile.name)
        fout = open(full_filename, 'wb+')
        fout.write(self.docfile.file.read())
        fout.close()

    def __json__(self):
        print("fd")
        data = dict()
        data['model'] = 'Document'
        data['docfile'] = self.docfile.name
        return data

    def _get_all(self):
        print('fafaffaada')
        table = db.table('Document')
        docs = []
        for item in table:
            docs.append(Document(docfile=os.path.join('documents/',item['docfile'])))
        return docs


class Picture(BaseModel):
    picture = models.ImageField(upload_to='pictures/')
    author = models.ForeignKey('Profile', on_delete=models.SET_NULL, null=True)
    description = models.TextField(max_length=200,null=True)
    def delete(self, *args, **kwargs):
        os.remove(os.path.join(MEDIA_ROOT, self.picture.name))
        super(Picture, self).delete(*args, **kwargs)

    def _save(self, *args, **kwargs):
        table = db.table('Picture')
        full_filename = os.path.join(MEDIA_ROOT, 'pictures/', self.picture.name)
        print(full_filename)
        table.insert(dict((self.__json__(full_filename=full_filename))))
        fout = open(full_filename, 'wb+')
        # Iterate through the chunks.
        fout.write(self.picture.file.read())
        fout.close()

    def __json__(self, full_filename):
        print("fd")
        data = dict()
        data['model'] = 'Picture'
        data['picture'] = self.picture.name
        data['author'] = self.author.name
        data['description'] = self.description
        return data

    def _get_all(self):
        print('fafaffaada')
        table = db.table('Picture')
        pics = []
        for item in table:
            pics.append(Picture(picture=os.path.join('pictures/',item['picture']),
                                description=item['description']))
            print(item['picture'])
        return pics
