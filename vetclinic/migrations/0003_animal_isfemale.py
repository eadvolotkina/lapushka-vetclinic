# Generated by Django 2.1.4 on 2018-12-11 18:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vetclinic', '0002_auto_20181211_2155'),
    ]

    operations = [
        migrations.AddField(
            model_name='animal',
            name='isFemale',
            field=models.BooleanField(default=True),
        ),
    ]
