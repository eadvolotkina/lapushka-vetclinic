# Generated by Django 2.1.4 on 2018-12-12 05:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vetclinic', '0004_auto_20181211_2205'),
    ]

    operations = [
        migrations.CreateModel(
            name='Shedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('monday', models.CharField(max_length=20)),
                ('tuesday', models.CharField(max_length=20)),
                ('wednesday', models.CharField(max_length=20)),
                ('thursday', models.CharField(max_length=20)),
                ('friday', models.CharField(max_length=20)),
                ('saturday', models.CharField(max_length=20)),
                ('sunday', models.CharField(max_length=20)),
            ],
        ),
        migrations.RemoveField(
            model_name='pet',
            name='isFemale',
        ),
        migrations.AddField(
            model_name='vetdoctor',
            name='shedule',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='vetclinic.Shedule'),
        ),
    ]
