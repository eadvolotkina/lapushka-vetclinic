from django.conf.urls import url
from django.urls import path

import vetclinic
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('facilities/', views.facilities, name='facilities'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact'),
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('profile/', views.profile, name='profile'),
    path('price/', views.price, name='price'),
    path('picture_delete/<int:pk>',views.picture_delete,name='picture_delete'),
    path('picture_change/<int:pk>', views.picture_change, name='picture_change'),
    path('specialty/searchdoctors', views.search_doctors, name='search_doctors'),
]
handler404 = vetclinic.views.handler404
