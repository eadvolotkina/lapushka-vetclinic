from django.contrib import admin
from .models import Animal, Pet, Profile, Specialization, VetDoctor, Shedule, Document, Picture
# Register your models here.

admin.site.register(Animal)
admin.site.register(Pet)
admin.site.register(Profile)
admin.site.register(Specialization)
admin.site.register(VetDoctor)
admin.site.register(Shedule)
admin.site.register(Document)
admin.site.register(Picture)
