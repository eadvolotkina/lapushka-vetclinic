import json

from django.db.models import Model
from django.shortcuts import render
from django.urls import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import Animal, Pet, Profile, Specialization, VetDoctor, Document, Picture
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import SignupForm, DocumentForm, PictureForm
# Create your views here.

def index(request):
    """
    Функция отображения для домашней страницы сайта.
    """
    specialiazations = Specialization.objects.all()
    # Отрисовка HTML-шаблона index.html с данными внутри
    # переменной контекста context
    return render(
        request,
        'index.html',
        context={'specs': specialiazations},
)

def facilities(request):
    """
    Функция отображения для страницы с описанием отделений.
    """
    return render(
        request,
        'facilities.html',
        context={},
)

def gallery(request):
    """
    Функция отображения для страницы с галереей.
    """
    # Handle file upload
    if request.method == 'POST':
        form = PictureForm(request.POST, request.FILES)
        if form.is_valid():
            newpic = Picture(picture = request.FILES['picture'],description=form.cleaned_data['description'],
                             author=Profile.objects.get(user=request.user))
            newpic.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('gallery'))
    else:
        form = PictureForm() # A empty, unbound form

    pictures = Picture(picture='dasaadsa').get_all()

    # Render list page with the documents and the form
    return render(request, 'gallery.html', {'pictures': pictures, 'form': form})


def contact(request):
    """
    Функция отображения для страницы с контактной информацией.
    """
    return render(
        request,
        'contact.html',
        context={},
)


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignupForm()
    return render(request, 'signup.html', {'form': form})

def profile(request):
    """
    Функция отображения для страницы личного кабинета.
    """
    if request.user.is_authenticated:
        return render(
            request,
            'profile.html',
            context={},
        )
    else:
        return HttpResponseRedirect('/vetclinic')


def price(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()
            return HttpResponseRedirect(reverse('price'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document(docfile='dasaadsa').get_all()

    # Render list page with the documents and the form
    return render(request, 'price.html', {'documents': documents, 'form': form})

def picture_delete(request, pk):
    if request.user.is_authenticated:
        pic = Picture.objects.get(pk=pk)
        if pic.author.user==request.user:
            if request.method=='POST':
                pic.delete()
                return redirect('gallery')
            return render(request, 'confirm_delete.html',{'picture':pic})
        else:
            return redirect('gallery')
    else:
        return redirect('gallery')

def picture_change(request, pk):
    if request.user.is_authenticated:
        pic = Picture.objects.get(pk=pk)
        if (pic.author.user == request.user):
            form = PictureForm(request.POST or None, instance=pic)
            if form.is_valid():
                if bool(request.FILES.get('picture', False)) == True:
                    pic.picture = request.FILES['picture']
                pic.description = form.cleaned_data['description']
                pic.save()
                return redirect('gallery')
        return render(request, 'picture_change.html', {'form':form})
    else:
        return redirect('gallery')

def search_doctors(request):
    specialty = request.GET.get('speciality')
    if specialty == "Все":
        doctors = list(VetDoctor.objects.all().values())
    else:
        doctors = list(VetDoctor.objects.filter(specialization = Specialization.objects.get(name=specialty)).values())
    print(len(doctors))
    data = dict()
    data['doctors'] = doctors
    return JsonResponse(data)

def handler404(request):
    return render(request, '404.html', status=404)