from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from vetclinic.models import Picture
from phonenumber_field.modelfields import PhoneNumberField


class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class DocumentForm(forms.Form):
    docfile = forms.FileField(
    label='Выберите файл'
)

class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = ['description','picture']